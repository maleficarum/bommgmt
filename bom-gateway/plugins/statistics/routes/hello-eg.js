module.exports = function (gatewayExpressApp) {
  gatewayExpressApp.get('/', (req, res) => {
    res.json({hello: 'Express-Gateway'});
  });
};
