const Influx = require('influx');
const influx = new Influx.InfluxDB({
  host: 'influxdb',
  database: 'bom_db',
  schema: [
    {
      measurement: 'response_times',
      fields: {
        apiEndpoint: Influx.FieldType.STRING,
        statusCode: Influx.FieldType.INTEGER,
        url: Influx.FieldType.STRING,
        duration: Influx.FieldType.INTEGER
      },
      tags: [
        'api'
      ]
    }
  ]
});

influx.getDatabaseNames().then(names => {
    if (!names.includes('bom_db')) {
      return influx.createDatabase('bom_db');
    }
  }).then((error) => {
    console.log("Created influx db ");
  }).catch(err => {
    console.error(`Error creating Influx database!`);
  });

module.exports = {
  name: 'statistics',
  policy: (actionParams) => {
    return (req, res, next) => {
      const start = Date.now();

      res.once('finish', () => {
        const apiEndpoint = req.egContext.apiEndpoint.apiEndpointName;
        const statusCode = res.statusCode;
        const url = req.originalUrl;

        influx.writePoints([
          {
            measurement: 'response_times',
            tags: { api: apiEndpoint },
            fields: { 
              apiEndpoint: apiEndpoint,
              statusCode: statusCode,
              url: url,
              duration: Date.now() - start
             },
          }
        ]).catch(err => {
          console.error(`Error saving data to InfluxDB! ${err.stack}`)
        });        

      });
      next();
    };
  }
};
