const common = require('../../backend/common/lib/common');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const express = require('express');
const exphbs  = require('express-handlebars');
const bodyParser = require('body-parser');
const app = express();
const request = require('request');

var config;
var log;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');
app.use(helmet());
app.use(cookieParser());

app.use(session({
    key: 'dh7_id',
    secret: 'DH7L@bs$.',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 1800000
    }
}));

const self = module.exports = {
    config: (__config__) => {
        config = __config__;

        log = common.getLogger("web");

        app.engine('handlebars', exphbs({
            defaultLayout: 'main',
            layoutsDir: 'views/layouts',
            partialsDir: 'views/partials',
            helpers:{
              endOfList:(a, b) => {
                return a == b ? "" : ",";
              },
              dateFormat: require('handlebars-dateformat')
            }
          }));
      
          app.set('view engine', 'handlebars');
          app.disable('etag').disable('x-powered-by');
      
          app.use(express.static('public'));
      
      //    app.use('/log', express.static('public/'),serveIndex('public/', {'icons': true}))
      
          app.set('views', 'views');
      
    },
    start: () => {
        app.listen(config.http.port, () => {
            log.info('Listening on port ' + config.http.port);
        });

        app.get('/', (req, res) => {
            res.render('index');
        });

        app.get('/projects', (req, res) => {
            var options = {
                url: config.servicesHost + ':10001/projects/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                }
            };
            
            request.get(options, function (error, response, body) {
                const projects = JSON.parse(body).projects;

                if(error) {
                  log.error("Error fetching projects ", error);
                } else {
                  
                }

                res.render('projects', {"projects": projects});
            });               
        });

        app.post('/projects/save', (req, res) => {
            log.trace("Request ", req.body);

            var options = {
                url: config.servicesHost + ':10001/projects/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                },
                body: {
                    "name":req.body.name,
                    "schematic":req.body.schematic,
                    "git":req.body.git,
                    "id": parseInt(req.body.id)
                },
                json: true
            };
            
            request.post(options, function (error, response, body) {
                if(error) {
                  log.error("Error saving project ", error);
                } else {
                    res.redirect('/projects');                  
                }

            });   
        });

        app.get('/types', (req, res) => {
            var options = {
                url: config.servicesHost + ':10000/components/types/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                }
            };
            
            request.get(options, function (error, response, body) {
                const types = JSON.parse(body).types;

                if(error) {
                  log.error("Error fetching types ", error);
                } else {
                  
                }

                res.render('types', {"types": types});
            });               
        });                

        app.post('/types/save', (req, res) => {
            log.trace("Request ", req.body);
            var options = {
                url: config.servicesHost + ':10000/components/types/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                },
                body: {
                    "name":req.body.name,
                    "id": parseInt(req.body.id)
                },
                json: true
            };
            
            request.post(options, function (error, response, body) {
                if(error) {
                  log.error("Error saving component type ", error);
                } else {
                    res.redirect('/types');                  
                } 

            });   
        });

        app.get('/packages', (req, res) => {
            var options = {
                url: config.servicesHost + ':10000/components/package/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                }
            };
            
            request.get(options, function (error, response, body) {
                const packages = JSON.parse(body).packages;

                if(error) {
                  log.error("Error fetching packages ", error);
                } else {
                  
                }

                res.render('packages', {"packages": packages});
            });               
        });    
        
        app.post('/packages/save', (req, res) => {

            var options = {
                url: config.servicesHost + ':10000/components/package/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                },
                body: {
                    "name":req.body.name,
                    "id": parseInt(req.body.id)
                },
                json: true
            };
            
            request.post(options, function (error, response, body) {
                if(error) {
                  log.error("Error saving component type ", error);
                } else {
                    res.redirect('/packages');                  
                } 

            });   
        });        
        //Components
        app.get('/components', (req, res) => {
            var options = {
                url: config.servicesHost + ':10000/components/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                }
            };
            
            request.get(options, function (error, response, body) {
                const components = JSON.parse(body).components;

                if(error) {
                  log.error("Error fetching components ", error);
                }

                self.getCatalog(':10000/components/package/', (error, response) => {
                    self.getCatalog(':10000/components/types/', (error, response2) => {
                        res.render('components', {"components": components, "packages": response.packages, "types": response2.types});
                    });
                });

            });               
        });    

        app.post('/components/save', (req, res) => {
            var options = {
                url: config.servicesHost + ':10000/components/',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                },
                body: { 
                    "id": req.body.id,
                    "description": req.body.description,
                    "value": req.body.value,
                    "part": req.body.part,
                    "datasheet": req.body.datasheet,
                    "quantity": parseInt(req.body.quantity),
                    "type": parseInt(req.body.type),
                    "package": parseInt(req.body.package)
                },
                json: true
            };
            
            request.post(options, function (error, response, body) {
                const components = body;

                if(error) {
                  log.error("Error fetching components ", error);
                }

                res.redirect('/components');
            });  
        });

        app.get('/api/components', (req, res) => {
            self.getCatalog(':10000/components/', (error, response) => {
                var components = [];

                response.components.forEach((v, i) => {
                    components.push({
                        "value": v.ID,
                        "text": v.DESCRIPTION + " [ " + v.VALUE + " / " + v.PACKAGE + " ] "
                    })
                });
                res.send(components);
            });            
        });

        app.get('/api/components/byproject/:project', (req, res) => {
            self.getCatalog(':10000/components/byproject/' + req.params.project, (error, response) => {
                res.send(response.components);
            });
        });        

        app.post('/api/project/assignComponent', (req, res) => {
            log.debug("Adding component to project ", req.body);

            var options = {
                url: config.servicesHost + ':10001/projects/assignComponent',
                headers: {
                  "Content-type": "application/json",
                  "Accept-Version": "1.0.0"
                },
                body: { 
                    "project": parseInt(req.body.project),
                    "component": parseInt(req.body.component)
                },
                json: true
            };
            
            request.post(options, function (error, response, body) {
                const components = body;

                if(error) {
                  log.error("Error assigning componet to project ", error);
                }

                res.send(components);
            }); 
        });        
    },
    getCatalog: (context, callback) => {
        var options = {
            url: config.servicesHost + context,
            headers: {
              "Content-type": "application/json",
              "Accept-Version": "1.0.0"
            }
        };
        
        request.get(options, function (error, response, body) {
            if(error) {
                callback(error, undefined);
            } else {
                callback(undefined, JSON.parse(body));
            }
        });           
    }
};