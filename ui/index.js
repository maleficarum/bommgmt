const http = require('./lib/http.js')
const config = require('./config/config.json');

http.config(config);
http.start();