"use strict";

const VERSION = "1.0.0";
const common = require('../../../common/lib/common');
const log = common.getLogger("projects-api");

const self = module.exports = {
    version: () => {
        return VERSION;
    },
    root: (req, res, next) => {
        return next();
    },
    projects: {
        get: (req, res, next) => {
            common.db.query("SELECT * FROM PROJECT ORDER BY CREATION_DATE, NAME", []).then(results => {
                res.send({ projects: results});
                return next();
            }).catch(error => {
                log.error("Error looking for projects ", error);
                res.status(500);
                res.send({});
                return next();
            });                
        },
        createOrUpdate: (req, res, next) => {
            const id = req.body.id;
            const name = req.body.name.toUpperCase();
            const schematic = req.body.schematic.toUpperCase();
            const git = req.body.git.toUpperCase();


            if(!id) {
                log.debug("Saving new project ", name);

                common.db.query("INSERT INTO PROJECT (NAME, SCHEMATIC_VERSION, GIT_REPO) VALUES (?, ?, ?)", [name, schematic, git]).then(results => {
                    res.send({ projects: results});
                    return next();
                });
            } else {
                log.debug("Updating project id ", id);
                
                common.db.query("UPDATE PROJECT set NAME = ?, SCHEMATIC_VERSION = ?, GIT_REPO = ? WHERE ID = ?", [name, schematic, git, id]).then(results => {
                    res.send({ projects: results});
                    return next();
                });
            } 
        },
        delete: (req, res, next) => {
            const id = req.params.id;

            common.db.query("DELETE FROM PROJECT WHERE ID = ?", [id]).then(results => {
                res.send({ projects: results});
                return next();
            });
        },
        assignComponent: (req, res, next) => {

            common.db.query("INSERT INTO COMPONENT_has_PROJECT (PROJECT_ID, COMPONENT_ID) VALUES (?, ?)", [req.params.project, req.params.component]).then(results => {
                common.db.query("SELECT C.* FROM BOM.COMPONENT C JOIN COMPONENT_has_PROJECT CP ON (CP.COMPONENT_ID = C.ID) JOIN PROJECT P ON (P.ID = CP.PROJECT_ID) WHERE P.ID = ?", [req.params.project]).then(results2 => {
                    res.send({components: results2});
                    return next();
                });
            }).catch(error => {
                log.error("Error assigning component to project ", req.params);
                res.send({});
            });
        }
    }

};