let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
//let server = require('../index');

const URL = "http://localhost:10001";

chai.use(chaiHttp);

describe('Test projects', () => {
    
    it('Should create or update project', (done) => {
        chai.request(URL).post('/projects/').set('Accept-Version','1.0.0').set('Accept-Language','application/json').send(
            {
                "name":"PetTracker",
                "schematic":"",
                "git":""
            }
            ).end((err, res) => {
        
            if(err) {
                console.error(err);
            } else {
                res.should.have.status(200);
            }

            done();
        });
    });      


    it('Should get projects', (done) => {
        chai.request(URL).get('/projects/').set('Content-header','application/json').end((err, res) => {
        
            if(err) {
                console.error(err);
            }
            
            res.should.have.status(200);

            console.log(res.body);

            done();
        });
    });    
    
    it('Should delete project', (done) => {
        chai.request(URL).delete('/projects/1').set('Accept-Version','1.0.0').set('Accept-Language','application/json').end((err, res) => {
        
            if(err) {
                console.error(err);
            } else {
                res.should.have.status(200);
            }

            done();
        });
    });        
});