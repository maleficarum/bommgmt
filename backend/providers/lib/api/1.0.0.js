"use strict";

const VERSION = "1.0.0";

const self = module.exports = {
    version: () => {
        return VERSION;
    },
    root: () => {
        return "";
    }
};