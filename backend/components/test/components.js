let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
let server = require('../index');

const URL = "http://localhost:10000";

chai.use(chaiHttp);

describe('Test components', () => {

    it('Should get components', (done) => {
        chai.request(URL).get('/components/').set('Content-header','application/json').end((err, res) => {
        
            if(err) {
                console.error(err);
            }
            
            res.should.have.status(200);

            done();
        });
    });

    it('Should get component by id', (done) => {
        chai.request(URL).get('/components/5').set('Content-header','application/json').end((err, res) => {
        
            if(err) {
                console.error(err);
            }
            
            res.should.have.status(200);

            done();
        });
    });

    it('Should get component types', (done) => {
        chai.request(URL).get('/components/types/').set('Content-header','application/json').end((err, res) => {
        
            if(err) {
                console.error(err);
            }
            
            res.should.have.status(200);

            done();
        });
    });    

    it('Should get component type by id', (done) => {
        chai.request(URL).get('/components/types/1').set('Content-header','application/json').end((err, res) => {
        
            if(err) {
                console.error(err);
            }
            
            res.should.have.status(200);

            done();
        });
    });   
    
    it('Should create or update component type', (done) => {
        chai.request(URL).post('/components/types/').set('Accept-Version','1.0.0').set('Accept-Language','application/json').send({"name":"Capacitor5", "id":5 }).end((err, res) => {
        
            if(err) {
                console.error(err);
            } else {
                res.should.have.status(200);
            }

            done();
        });
    });   
    
    it('Should get packages', (done) => {
        chai.request(URL).get('/components/package/').set('Content-header','application/json').end((err, res) => {
        
            if(err) {
                console.error(err);
            }
            
            res.should.have.status(200);

            done();
        });
    });       
});