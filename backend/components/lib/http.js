"use strict";

const common = require('../../common/lib/common.js');
const restify = require('restify');
const walk = require('walk-sync');
const fs = require('fs');
//Security libs
const cors = require('cors');
const helmet = require('helmet');
const hide = require('hide-powered-by');
const consul = require('./consul');

const MODULE_NAME = "common-http";
var config;
var log;
var server;
var apis = [];

const self = module.exports = {
    config : (__config__) => {
        config = __config__;

        log = common.getLogger(MODULE_NAME);

        server = restify.createServer();

        server.opts("/.*/", function(req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', "GET, POST");
            res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers'));
            res.send(200);
            return next();
        });
    
        server.use(cors({
            "origin": "*",
            "methods": "GET,HEAD,PUT,PATCH,POST,DELETE"
        }));      
    
        server.use(restify.plugins.bodyParser({ mapParams: true }));
        server.use(helmet());
        server.use(hide({ setTo: 'DH7/PHP 6.6.6' }));

        walk('./lib/api').forEach((file) => {
            var api = require('./api/' + file);
            apis.push(api);

            log.info("Added API version ",api.version());

            server.get({path: "/", version: api.version()}, api.root);

            server.get({path: "/components/:componentId", version: api.version()}, api.components.get);
            server.get({path: "/components/byproject/:project", version: api.version()}, api.components.getByProjectId);
            server.post({path: "/components/", version: api.version()}, api.components.createOrUpdate);

            server.get({path: "/components/types/:typeId", version: api.version()}, api.components.types.get);
            server.post({path: "/components/types/", version: api.version()}, api.components.types.createOrUpdate);

            server.get({path: "/components/package/:packageId", version: api.version()}, api.components.package.get);
            server.post({path: "/components/package/", version: api.version()}, api.components.package.createOrUpdate);

            //Healthcheck
            server.get({path: "/health", version: api.version()}, api.healtcheck);
        });

        common.db.connect(config);

        consul.init({
            "service":"Components"
        });
    },
    start: () => {
        log.info("Listening on port ", config.http.port);
        server.listen(config.http.port);
    }
};