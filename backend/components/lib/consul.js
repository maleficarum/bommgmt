const consul = require('consul')();
const os = require('os');
const common = require('../../common/lib/common.js');
const PID = process.pid;
const PORT = Math.floor(process.argv[2]);
const HOST = os.hostname();
const CONSUL_ID = `data-${HOST}-${PORT}-${require('uuid').v4()}`;
const dns = require("dns");

var log = common.getLogger("consul");

const self = module.exports = {
    init: (config) => {

        consul.agent.service.register({
            name: config.service,
            address: HOST,
            port: PORT,
            id: CONSUL_ID,
            check: {
              ttl: '10s',
              deregister_critical_service_after: '1m'
            }            
        }, err => {
            if(err) {
                log.error("Error registrering on consul ", err);
            }

            setInterval(() => {
                consul.agent.check.pass({id:`service:${CONSUL_ID}`}, err => {
                  if (err) {
                      log.error("Error pinging consul", err);
                  }
                });
            }, 5 * 1000);   
              
              /*let known_data_instances = [];


              const watcher = consul.watch({
                method: consul.health.service,
                options: {
                  service: 'data',
                  passing: true
                }
              });  
              
              watcher.on('change', data => {
                known_data_instances = [];
                data.forEach(entry => {
                  known_data_instances.push(`http://${entry.Service.Address}:${entry.Service.Port}/`);

                  console.log(known_data_instances);
                });
              });              

              process.on('SIGINT', () => {
                console.log('SIGINT. De-Registering...');
                let details = {id: CONSUL_ID};
              
                consul.agent.service.deregister(details, (err) => {
                  console.log('de-registered.', err);
                  process.exit();
                });
              });*/
        });

        dns.setServers(['127.0.0.1:8600']);

    },
    getService: (serviceName, callback) => {
        dns.resolveSrv(serviceName + ".service.consul", function(err, records){
            callback(err, records);
        });        
    }
};