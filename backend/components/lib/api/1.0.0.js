"use strict";

const VERSION = "1.0.0";
const common = require('../../../common/lib/common.js');

var log = common.getLogger('api-' + VERSION);

const self = module.exports = {
    version: () => {
        return VERSION;
    },
    root: (req, res, next) => {
        res.send({ version: VERSION});
        return next();
    },
    healtcheck: (req, res, next) => {
        res.send("ok");
        return next();
    },
    components: {
        get: (req, res, next) => {
            const componentId = req.params.componentId;

            if(componentId) {
                common.db.query("SELECT c.ID, c.DESCRIPTION, c.VALUE, c.PART_NO, c.QUANTITY, c.DATASHEET, ct.NAME AS TYPE, p.NAME AS PACKAGE FROM COMPONENT c JOIN COMPONENT_TYPE ct ON (ct.ID = c.COMPONENT_TYPE_ID) JOIN PACKAGE p ON (p.ID = c.PACKAGE_ID) WHERE c.ID = ? ORDER BY TYPE, VALUE, PACKAGE, QUANTITY", [componentId]).then( results => {
                    res.send({ components: results});
                    return next();
                });
            } else {
                common.db.query("SELECT c.ID, c.DESCRIPTION, c.VALUE, c.PART_NO, c.QUANTITY, c.DATASHEET, ct.NAME AS TYPE, p.NAME AS PACKAGE FROM COMPONENT c JOIN COMPONENT_TYPE ct ON (ct.ID = c.COMPONENT_TYPE_ID) JOIN PACKAGE p ON (p.ID = c.PACKAGE_ID) ORDER BY TYPE, VALUE, PACKAGE, QUANTITY", []).then( results => {
                    res.send({ components: results});
                    return next();
                });
            }
        },
        getByProjectId: (req, res, next) => {
            const project = req.params.project;

            log.debug("Looking for components for the project " + project);

            common.db.query("SELECT C.DESCRIPTION, C.VALUE, C.PART_NO, C.QUANTITY, CT.NAME AS COMPONENT_TYPE, PK.NAME AS PACKAGE FROM BOM.COMPONENT C JOIN COMPONENT_has_PROJECT CP ON (CP.COMPONENT_ID = C.ID) JOIN PROJECT P ON (P.ID = CP.PROJECT_ID) JOIN COMPONENT_TYPE CT ON (CT.ID = C.COMPONENT_TYPE_ID) JOIN PACKAGE PK ON (PK.ID = C.PACKAGE_ID) WHERE P.ID = ?", [project]).then( results => {
                res.send({ components: results});
                return next();
            });            
        },
        createOrUpdate: (req, res, next) => {
            const desc = req.body.description.toUpperCase();
            const value = req.body.value.toUpperCase();
            const part = req.body.part.toUpperCase();
            const datasheet = req.body.datasheet.toUpperCase();
            const quantity = req.body.quantity;
            const type = req.body.type;
            const pckage = req.body.package;
            const id = req.body.id;
            const provider = 1;

            if(!id) {
                common.db.query("INSERT INTO COMPONENT (DESCRIPTION, VALUE, PART_NO, PROVIDER_ID, QUANTITY, COMPONENT_TYPE_ID, PACKAGE_ID, DATASHEET) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [desc, value, part, provider, quantity, type, pckage, datasheet]).then(results => {
                    res.send({ components: results});
                    return next();
                }).catch(error => {
                    log.error("Error inserting component ", error);
                    res.send({"error":"Error inserting component"});
                });
            } else {
                common.db.query("UPDATE COMPONENT_TYPE set NAME = ? WHERE ID = ?", [typeName, typeId]).then(results => {
                    res.send({ components: results});
                    return next();
                }).catch(error => {
                    log.error("Error updating component ", error);
                    res.send({"error":"Error updating component"});
                });
            }             
        },
        delete: () => {},
        types: {
            get: (req, res, next) => {
                //const typeName = JSON.parse(req.body).name.toUpperCase();
                common.db.query("SELECT * FROM COMPONENT_TYPE ORDER BY NAME", []).then(results => {
                    res.send({ types: results});
                    return next();
                });                
            },
            createOrUpdate: (req, res, next) => {
                const typeName = req.body.name.toUpperCase();
                const typeId = req.body.id;

                if(!typeId) {
                    common.db.query("INSERT INTO COMPONENT_TYPE (NAME) VALUES (?)", [typeName]).then(results => {
                        res.send({ components: results});
                        return next();
                    });
                } else {
                    common.db.query("UPDATE COMPONENT_TYPE set NAME = ? WHERE ID = ?", [typeName, typeId]).then(results => {
                        res.send({ components: results});
                        return next();
                    });
                }                
            },
            delete: (req, res, next) => {}
        },
        package: {
            get: (req, res, next) => {
                common.db.query("SELECT * FROM PACKAGE ORDER BY NAME", []).then(results => {
                    res.send({ packages: results});
                    return next();
                });                                
            },
            createOrUpdate: (req, res, next) => {
                const name = req.body.name.toUpperCase();
                const id = req.body.id;

                if(!id) {
                    log.debug("Creating new package ", req.body);

                    common.db.query("INSERT INTO PACKAGE (NAME) VALUES (?)", [name]).then(results => {
                        res.send({ packages: results});
                        return next();
                    });
                } else {
                    log.debug("Updating package", req.body);

                    common.db.query("UPDATE PACKAGE set NAME = ? WHERE ID = ?", [name, id]).then(results => {
                        res.send({ packages: results});
                        return next();
                    });
                }
                
                return next();
            },
            delete: (req, res, next) => {}
        }
    }
};