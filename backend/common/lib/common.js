"use strict";

const bunyan = require("bunyan");
const mysql = require('mysql');

var connection;

const self = module.exports = {
    getLogger : (moduleName) => {
        return bunyan.createLogger({
            name: moduleName,
            src: false,
            streams: [
                {
                    level:'trace',
                    stream: process.stdout
                },
                {
                    level: 'debug',
                    path: '/tmp/' + moduleName + ".log"
                },
                {
                    level: 'error',
                    path: '/tmp/' + moduleName + ".err"
                }
              ]
        });
    },
    db : {
        connect: (config) => {
            connection = mysql.createPool(config.db);
        },
        disconnect: () => {
            if(connection) {
                connection.end(() => {

                });
            }
        },
        query: (query, args) => {
            return new Promise((resolve, reject) => {
                connection.query(query, args, (error, results, fields) => {
                    if(error) {
                        reject(error);
                    } else {
                        resolve(results);    
                    }
                });      
            });         
        }
    }
};